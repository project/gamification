<?php

namespace Drupal\Tests\gamification\Unit\Entity;

use Drupal\Tests\UnitTestCase;
use Drupal\gamification\Entity\GamificationEntity;

/**
 * Unit tests for Gamification class.
 *
 * @ingroup gamification
 *
 * @group gamification
 *
 * @coversDefaultClass \Drupal\gamification\Entity\GamificationEntity
 */
class GamificationEntityTest extends UnitTestCase {

  /**
   * Tests the Gamification entity functions.
   */
  public function testEntity() {
    // Mock a Gamification entity.
    $entity = new GamificationEntity([], 'gamification');
    $this->assertTrue($entity instanceof GamificationEntity);

    // Test entity methods.
    $entity->setTitle('testTitle');
    $this->assertEquals($entity->getTitle(), 'testTitle');

    $entity->setWidgetValue('testWidget');
    $this->assertEquals($entity->getWidgetValue(), 'testWidget');

    $entity->setAction('testAction');
    $this->assertEquals($entity->getAction(), 'testAction');

    $entity->setPoints('50');
    $this->assertEquals($entity->getPoints(), '50');
  }

}
