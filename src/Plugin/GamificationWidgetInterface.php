<?php

namespace Drupal\gamification\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\gamification\GamificationInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface for the Gamification widget.
 */
interface GamificationWidgetInterface extends PluginInspectionInterface {

  /**
   * Check if a plugin should be visible in gamification configurations.
   */
  public function checkAccess();

  /**
   * Action handler.
   *
   * @param \Drupal\gamification\GamificationInterface $gamification_config_entity
   *   Gamification configuration entity.
   * @param string $method
   *   Triggering event string.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity which triggered this event.
   *
   * @return bool
   *   True if executed with success, otherwise return false.
   */
  public function execute(GamificationInterface $gamification_config_entity, $method, ContentEntityInterface $entity);

  /**
   * Limit attribution points according to time, entity id or bundle.
   *
   * @param \Drupal\gamification\GamificationInterface $gamification_config_entity
   *   Gamification configuration entity.
   * @param string $method
   *   Triggering event string.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity which triggered this event.
   *
   * @return bool
   *   True if limits are respected, otherwise return false.
   */
  public function checkLimits(GamificationInterface $gamification_config_entity, $method, ContentEntityInterface $entity);

  /**
   * Get gamification log with specific conditions.
   *
   * @param array $values
   *   Values to be used in conditions.
   * @param array $fields
   *   Fields to be returned from database.
   *
   * @return mixed
   *   Result obtained from database.
   */
  public function getGamificationLogByValues(array $values, array $fields);

  /**
   * Create gamification log after triggering.
   *
   * Some configured gamification event.
   *
   * @param \Drupal\gamification\GamificationInterface $gamification_config_entity
   *   Gamification configuration entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity which triggered this event.
   * @param string $method
   *   Triggering event string.
   * @param int $points
   *   Points that must be attributed to user.
   *
   * @return bool
   *   True if created log with success, otherwise should return false.
   */
  public function createGamificationLog(GamificationInterface $gamification_config_entity, ContentEntityInterface $entity, $method, $points);

}
